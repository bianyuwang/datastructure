package com.example.studentinformation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText studentid,studentname,studentage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize(){
              studentid=findViewById(R.id.studentid);

                studentname=findViewById(R.id.studentname);
                studentage=findViewById(R.id.studentage);


    }
    public void doshowinfo(View view) {

String studentidstr=studentid.getText().toString();
String studentnamestr=studentname.getText().toString();
String studentagestr=studentage.getText().toString();


        Toast.makeText(this,
               "Id:"+studentidstr+"\n"+
                "Name: "+studentnamestr+"\n"+
                "Age: "+studentagestr,
                Toast.LENGTH_LONG).show();

    }
}
