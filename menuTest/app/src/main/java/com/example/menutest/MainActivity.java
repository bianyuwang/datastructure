package com.example.menutest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        SubMenu intentSubmenu= menu.addSubMenu("intent options");
        intentSubmenu.add("Open Browser").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.google.com"));
                startActivity(intent);
                return false;
            }
        }

        );
        intentSubmenu.add("Open SMS").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent= new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("sms:5819225504"));

                intent.putExtra("sms_body","Hello from android application");
                startActivity(intent);

                return false;
            }
        });
        intentSubmenu.add("Open dialer").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Intent intent= new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("tel:5819225504"));
                startActivity(intent);
                return false;
            }
        });

        intentSubmenu.add("Open camera").setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        Intent intent = new Intent(MainActivity.this, CameraUse.class);
                        MainActivity.this.startActivity(intent);
                        return false;

                    }
                });

        return super.onCreateOptionsMenu(menu);
    }


}
