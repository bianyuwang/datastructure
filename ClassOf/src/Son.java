public class Son extends Parent {

   public Son(){
        super();
        System.out.println("constructor Son");
    }

    @Override
    public void setWeakness(String weakness) {
        super.setWeakness(weakness);
    }

    @Override
    public String getWeakness() {
        System.out.println("son's weakness ");
        return super.getWeakness();
    }

    @Override
    public String toString() {
        return "message from Son";
    }
}
