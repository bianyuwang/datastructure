public class GrandParent {
    public String Name;
    public int Age;
    public String  Genre;
    private String Weakness;
    protected String Job;

    GrandParent(){

        System.out.println("constructor GrandParent");

    }

    public void setName(String name) {
        Name = name;
    }

    public void setJob(String job) {
        Job = job;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public void setAge(int age) {
        Age = age;
    }

    public void setWeakness(String weakness) {

        Weakness = weakness;
    }

    public String getName() {
        return Name;
    }

    public String getJob() {
        return Job;
    }

    public String getGenre() {
        return Genre;
    }

    public int getAge() {
        return Age;
    }

    public String getWeakness() {
        System.out.println("GrandParent's weakness");
        return Weakness;
    }

    @Override
    public String toString() {
        return "message from Grand parents";
    }
}
