public class HashTable {
    private MyLinkedList[] hashArray;
    private static final int SIZE = 20;

    public HashTable()
    {
        hashArray = new MyLinkedList[SIZE];

        for (int i=0; i < SIZE; i++)
            hashArray[i] = new MyLinkedList();
    }

    private int computeHash(String s)
    {
        int hash = 0;
        for (int i = 0; i < s.length(); i++) {
            hash += s.charAt(i);
        }
        return hash % SIZE;
    }

    public int getKey(String s)
    {
        return computeHash(s);

    }


    public boolean containsString(String target)
    {
        int hash = computeHash(target);
        MyLinkedList list = hashArray[hash];
        if (list.contains(target))
            return true;
        return false;
    }

    public void put(String s)
    {
        int hash = computeHash(s);	// Get hash value
        MyLinkedList list = hashArray[hash];
        if (!list.contains(s))
        {
            hashArray[hash].addToStart(s);
        }
    }
}
