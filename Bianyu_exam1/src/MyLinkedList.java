public class MyLinkedList {

    MyNode head;

    public MyLinkedList(MyNode currentNode) {
        this.head = currentNode;
    }

    public MyLinkedList() {
    }

    public void addToStart(String newWord)
    {
        head = new MyNode(newWord, head);
    }

    public int size() {
        int count = 0;
        MyNode position = head;
        while (position != null) {
            count++;
            position = position.getNextNode();
        }
        return count;
    }


    public void showAllNode() {
        int count = 0;
        MyNode currentNode = head;
        while (currentNode != null) {
            count++;
            System.out.println(count + ".  " + currentNode.getObj1().toString());
            currentNode = currentNode.getNextNode();
        }

    }

    public boolean contains(String item) {
        return (find(item) != null);
    }

    private MyNode find(String target) {
        MyNode position = head;
        String itemAtPosition;
        while (position != null) {
            itemAtPosition = position.getObj1();
            if (itemAtPosition.equals(target))
                return position;
            position = position.getNextNode();
        }
        return null;

    }
}