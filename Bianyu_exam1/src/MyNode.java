import java.util.Objects;



    public class MyNode {


        private  String word;

        private MyNode nextWord;


        public MyNode(String word) {
            this.word = word;

        }

        public MyNode() {
        }

        public MyNode(String word,  MyNode nextNode) {
            this.word = word;

            this.nextWord = nextNode;
        }

        public String getObj1() {
            return word;
        }



        public void setData(String word){
            this.word=word;
        }



        @Override
        public int hashCode() {
            return Objects.hash(word);
        }

        @Override
        public String toString() {
            return "word :"+word;

        }

        public MyNode getNextNode() {
            return nextWord;
        }

        public void setNextNode(MyNode nextNode) {
            this.nextWord = nextNode;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyNode myNode = (MyNode) o;
            return Objects.equals(word, myNode.word) &&
                    Objects.equals(nextWord, myNode.nextWord);
        }
    }


