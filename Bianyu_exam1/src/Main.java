import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args)


    {

        long startTime=System.currentTimeMillis();   //begin time


        Scanner inputStream;
        int lineNumber=0;
        int rightLigne=0;
        HashTable hashTable = new HashTable();
        int max=0;
        try {
            inputStream = new Scanner(new FileInputStream("words.txt"));
            System.out.println("Read From File!");

            while (inputStream.hasNextLine()) {
                lineNumber++;
                String word = inputStream.nextLine();
                String pattern = ".*[a,e,i,o,u].*";
                boolean isMatch = Pattern.matches(pattern,word.toLowerCase());
                if(!isMatch)
                {
                    rightLigne++;
                  //  System.out.println(lineNumber+" -- "+rightLigne+". "+word+"-------------"+hashTable.getKey(word));
                if(hashTable.getKey(word)>max)
                {
                    max=hashTable.getKey(word);
                }

                   System.out.println(lineNumber+" -- "+rightLigne+". "+word);
                    hashTable.put(word);
                }


        } }catch (IOException e) {
            System.out.println("Can not open File");
        }
        finally {
            System.out.println(max);
            System.out.println("SQL in hash table? " + hashTable.containsString("SQL"));
            System.out.println("RVSP in hash table? " + hashTable.containsString("RVSP"));
            System.out.println("NYC in hash table? " + hashTable.containsString("NYC"));
            System.out.println("A5 in hash table? " + hashTable.containsString("A5"));
        }

        long endTime=System.currentTimeMillis(); //Finish time
        System.out.println("running time： "+(endTime-startTime)+"ms");
    }
}
