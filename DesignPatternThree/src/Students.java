public class Students implements Observer {

    private  int StudentId;
    public Students(int studentId){
        this.StudentId=studentId;
    }
    @Override
    public void update(String message) {
        System.out.println(StudentId+"-"+message);
    }
}
