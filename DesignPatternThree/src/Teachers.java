import java.util.ArrayList;
import java.util.List;

public class Teachers implements Subject {

  private ArrayList<Observer> studentsList= new ArrayList<Observer>();
    @Override
    public void attach(Observer observer) {
        studentsList.add(observer);
    }

    @Override
    public void detach(Observer observer) {
studentsList.remove(observer);
    }

    @Override
    public void notify(String message) {
for(Observer observer:studentsList)
{
    observer.update(message);
}
    }
}
