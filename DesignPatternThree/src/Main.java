public class Main {

    public static void main(String[] args)
    {
        Teachers teacherJava= new Teachers();
       Students s1=new Students(1896181);
       Students s2=new Students(1234567);
       Students s3= new Students(7654321);

       teacherJava.attach(s1);
       teacherJava.attach(s2);
       teacherJava.attach(s3);

       teacherJava.notify("new assignment upload");

       teacherJava.detach(s3);
        teacherJava.notify("one Student drop the class");
    }
}
