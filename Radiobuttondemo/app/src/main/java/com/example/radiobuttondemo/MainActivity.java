package com.example.radiobuttondemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    RadioGroup radioGroup;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }

    private void initialize(){

        radioGroup=findViewById(R.id.radiogroup);
        imageView=findViewById(R.id.imageview);

    }

    public void changeimage(View view) {

int selectedRadioBtn=radioGroup.getCheckedRadioButtonId();
switch(selectedRadioBtn) {
    case R.id.radio1:
        imageView.setImageResource(R.drawable.jerry);
        break;
    case R.id.radio2:
        imageView.setImageResource(R.drawable.jerrya);
        break;
    case R.id.radio3:
        imageView.setImageResource(R.drawable.jerryb);
        break;

}

    }
}
