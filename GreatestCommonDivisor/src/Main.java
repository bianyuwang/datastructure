import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("please enter first number");

        int num1 = sc.nextInt();
        System.out.println("please enter second number");
        int num2 = sc.nextInt();
        int Gcd;
        Gcd = getGCD(num1, num2);
        System.out.println("Gcd of this two numbers is: "+Gcd);
    }


    public static int getGCD(int a, int b) {
        if (a < 0 || b < 0) {
            return -1;
        }
        if (b == 0) {
            return a;
        }
        return a % b == 0 ? b : getGCD(b,a % b);
    }
}