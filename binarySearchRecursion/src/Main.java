public class Main {

    public static void main(String[] args) {
        int[] nums={1,2,3,4,5,6,7,8,9};

        System.out.println(findKey(nums,8,0,5));
    }

    public static int findKey(int[] nums, int high, int low,int key)
    {
        int mid=(high-low)/2;
        if(key==nums[mid])
            return mid;
        else{if (key<nums[mid])
        high=mid-1;
        else
            low=mid+1;

        return findKey(nums,high,low,key);
        }

    }
}
