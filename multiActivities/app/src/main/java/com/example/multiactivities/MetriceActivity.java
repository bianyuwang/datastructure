package com.example.multiactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MetriceActivity extends AppCompatActivity implements View.OnClickListener {

    Button convertBtn,returnBtn;
    EditText  meterEdit;
    TextView centimeterDisplay, kilometerDisplay, lastNameDisplayView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metrice);
        initialize();
       myGetIntent();
    }

    private void myGetIntent() {

        Intent intent=getIntent();
        String lastName=  intent.getStringExtra("lastName");

        lastNameDisplayView.setText(lastName);
    }

    private void initialize() {
        convertBtn=findViewById(R.id.converBtn);
        convertBtn.setOnClickListener(this);
        returnBtn=findViewById(R.id.finishBtn);
        returnBtn.setOnClickListener(this);

        meterEdit=findViewById(R.id.meterEdit);
        lastNameDisplayView=findViewById(R.id.lastName);
        centimeterDisplay=findViewById(R.id.CentimeterDisplay);
        kilometerDisplay=findViewById(R.id.KilometerDisplay);

    }

    @Override
    public void onClick(View view) {

        int btnId=view.getId();
        switch (btnId)
        {
            case R.id.converBtn:

                convert();
                break;
            case  R.id.finishBtn:
                finish();
                break;
        }

    }

    private void convert() {

        double centimeter= Double.valueOf(meterEdit.getText().toString())*100;
        centimeterDisplay.setText(String.valueOf(centimeter));

        double kilometer= Double.valueOf(meterEdit.getText().toString())/1000;
        kilometerDisplay.setText(String.valueOf(kilometer));
    }
}
