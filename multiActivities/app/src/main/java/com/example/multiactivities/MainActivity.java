package com.example.multiactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText lastNameEdit;
    Button temperatureBtn;
    Button metriceBtn;
    Button finishBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {
        lastNameEdit=findViewById(R.id.lastName);
        temperatureBtn=findViewById(R.id.tempratureBtn);
        temperatureBtn.setOnClickListener(this);
        metriceBtn=findViewById(R.id.metricBtn);
        metriceBtn.setOnClickListener(this);
        finishBtn=findViewById(R.id.finishBtn);
        finishBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int btnId=view.getId();
        switch ( btnId)
        {
            case R.id.tempratureBtn:
temperatureConversation();
                break;
          case  R.id.metricBtn:
             metriceConversation();
             break;

            case  R.id.finishBtn:
                Toast.makeText(MainActivity.this,
                        "button 3:click",
                        Toast.LENGTH_LONG).show();
                break;

        }
    }

    private void metriceConversation() {
        String lastName=lastNameEdit.getText().toString();
        Intent myIntent=new Intent(this,MetriceActivity.class);
        myIntent.putExtra("lastName",lastName);
        startActivity(myIntent);
    }

    private void temperatureConversation() {

        String lastName=lastNameEdit.getText().toString();
        Intent myIntent=new Intent(this,TemperatureActivity.class);
        myIntent.putExtra("lastName",lastName);
        startActivity(myIntent);


    }


}
