package com.example.multiactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TemperatureActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTemperature_c;
    TextView temperatureEdit;
    Button converBtn,finishBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);

        initialize();
    }

    private void initialize() {

        editTemperature_c=findViewById(R.id.temperatureEdit);
        temperatureEdit=findViewById(R.id.FahrenheitDisplay);
        converBtn=findViewById(R.id.converBtn);
        converBtn.setOnClickListener(this);
        finishBtn=findViewById(R.id.finishBtn);
        finishBtn.setOnClickListener(this);
    }

    public void convert(){

        float celsius =Float.valueOf(editTemperature_c.getText().toString());
        double result=((9.0/5.0)*celsius)+32;

        temperatureEdit.setText(String.valueOf(result));

    }

    @Override
    public void onClick(View view) {
        int btnId=view.getId();
        switch (btnId){
            case R.id.converBtn:
                    convert();
            break;
            case R.id.finishBtn:
                finish();
                break;

        }
    }
}
