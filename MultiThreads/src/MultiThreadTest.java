import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultiThreadTest {

    MultiThreadTest(){
        ExecutorService executor = Executors.newFixedThreadPool(3);
 TaskClass task1= new Task1("task1",100);
 TaskClass task2= new TaskClass("task2",50);
 TaskClass task3= new TaskClass("task3",80);

 executor.execute(task1);
 executor.execute(task2);
 executor.execute(task3);

 executor.shutdown();

    }
}
