

/* Write a recursive method to return the number of uppercase letters in a string. Write
 a test program that prompts the user to enter a string and displays the number of uppercase
 letters in the string.*/

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Please enter a String: ");
        Scanner sc= new Scanner(System.in);
        String str= sc.nextLine();
        System.out.println("Total Of Upper Case Letters is : "+ findUpperCase(str,0,0));
    }


    public static int findUpperCase(String str, int place,int totalOfUpperCase)
    {
        int len= str.length();

        if(place == len)
            return totalOfUpperCase;
        else
        {

            if (Character.isUpperCase(str.charAt(place)))
            {
                totalOfUpperCase++;
                          }
            place++;
            return findUpperCase(str,place,totalOfUpperCase);
        }
    }
}


