import java.util.Objects;

public class Entry {
    private String item;
    private  int count;


    @Override
    public String toString() {
        return "Item" +
                "item='" + item + '\'' +
                ", count=" + count ;
    }

    public Entry(String item, int count) {
        this.item = item;
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entry entry = (Entry) o;
        return count == entry.count &&
                Objects.equals(item, entry.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, count);
    }
}
