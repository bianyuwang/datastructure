import java.util.Objects;

public class GenericLinkedList<T>{


    private Node<T> head;
    private Node<T> end;

    public Node<T> getHead() {
        return head;
    }

    public void setHead(Node<T> head) {
        this.head = head;
    }

    public Node<T> getEnd() {
        return end;
    }

    public void setEnd(Node<T> end) {
        this.end = end;
    }

    public GenericLinkedList() {
    }

    public GenericLinkedList(Node<T> head, Node<T> end) {
        this.head = head;
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericLinkedList<?> that = (GenericLinkedList<?>) o;
        return Objects.equals(head, that.head) &&
                Objects.equals(end, that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, end);
    }

    static class Node<T>{

      private   T item;
      private Node<T> nextNode;


        public Node() {
        }

        public Node(T item, Node<T> nextNode) {
            this.item = item;
            this.nextNode = nextNode;
        }

        public Node(T item) {
            this.item = item;

        }

        public T getItem() {
            return item;
        }

        public void setItem(T item) {
            this.item = item;
        }

        public Node<T> getNextNode() {
            return nextNode;
        }

        public void setNextNode(Node<T> nextNode) {
            this.nextNode = nextNode;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return Objects.equals(item, node.item) &&
                    Objects.equals(nextNode, node.nextNode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(item, nextNode);
        }
    }



    public void addToStart(Node<T> node)
    {
        node.setNextNode(head);
        this.head=node;
    }

    public int size()
    {int count=0;
        Node<T> position=head;
        while (position!=null)
        {
            count++;
            position=position.getNextNode();

        }
        return count;
    }

    public void showAllNode(){
        int count=0;
        Node currentNode=head;
        while(currentNode!=null)
        {count++;
            System.out.println(count+".  "+ currentNode.getItem().toString());
            currentNode=currentNode.getNextNode();
        }

    }

    public void addToEnd(Node<T> node)

{
    if(size()==1)
    {
        this.head.nextNode=node;

    }

    else{
        this.end.nextNode=node;
    }
    this.end=node;
    this.end.nextNode=null;


}


}
