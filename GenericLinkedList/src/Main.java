
import java.util.Iterator;
import java.util.LinkedList;


public class Main {

    public static void main(String[] args) {

     GenericLinkedList<String> myLinkedList =new GenericLinkedList<>();


     myLinkedList.addToStart(new GenericLinkedList.Node<>( "test"));

     myLinkedList.addToEnd(new GenericLinkedList.Node<>("Generic"));

     myLinkedList.addToStart(new GenericLinkedList.Node<>( "now"));

     myLinkedList.showAllNode();


        LinkedList<String> stringLinkedList=new LinkedList<>();
        stringLinkedList.addFirst("now");
        stringLinkedList.addLast("test");
        stringLinkedList.addLast("Generic");
        Iterator<String> iterator = stringLinkedList.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }


    }
}
