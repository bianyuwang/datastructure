package com.example.testactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class Test1 extends AppCompatActivity implements View.OnClickListener {
 TextView viewQuestion;
 EditText editAnswer;
Button btnCancel, btnInitial,btnValide;

int result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);
        initialize();
    }

    private void initialize() {
        viewQuestion=findViewById(R.id.questionDisplay);
        editAnswer=findViewById(R.id.answerDisplay);

        btnCancel=findViewById(R.id.cancelBtn);
        btnCancel.setOnClickListener(this);

        btnInitial=findViewById(R.id.generateBtn);
        btnInitial.setOnClickListener(this);
        btnValide=findViewById(R.id.validateBtn);
        btnValide.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        int btnId=view.getId();
        switch (btnId){
            case R.id.CancelBtn:
                String strResult = "Operation canceled";

                //------------------------------------ Create an intent
                Intent intent = new Intent();
                intent.putExtra("cancel_tag", strResult);

                //------------------------------------ Set Result for MainActivity
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
            case R.id.generateBtn:
                generateQuestion();
                break;
            case R.id.validateBtn:
                validateAnswer();
                break;



        }

    }

    private void validateAnswer() {

        String strResult;
        if(result==Integer.valueOf(editAnswer.getText().toString())) {
            strResult = "Right Answer!";
        } else {
            strResult = "Wrong Answer!";
        }

        Intent intent = new Intent();
        intent.putExtra("return_result_tag", strResult);

        setResult(RESULT_OK, intent);
        finish();
        }


    private void generateQuestion() {
        Random rand = new Random();
        int a=rand.nextInt(10);
        int b=rand.nextInt(10);
        String randomQuestion=a+"+"+b+"=";

        viewQuestion.setText(randomQuestion);
        result= a+b;
    }
}
