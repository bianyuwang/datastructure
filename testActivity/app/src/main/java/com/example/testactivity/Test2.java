package com.example.testactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Test2 extends AppCompatActivity implements View.OnClickListener {

    Button okBtn, cancelBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        initialize();
    }

    private void initialize() {
        okBtn=findViewById(R.id.OkBtn);
                okBtn.setOnClickListener(this);
        cancelBtn=findViewById(R.id.CancelBtn);
        cancelBtn.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {

    }
}
