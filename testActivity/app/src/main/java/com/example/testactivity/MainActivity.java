package com.example.testactivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    final  static int REQUEST_CODE1 =1;
    final  static int REQUEST_CODE2 =2;
Button test1Btn,test2Btn;
TextView feedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {

        feedback=findViewById(R.id.answerDisplay);

        test1Btn=findViewById(R.id.test1Btn);
        test1Btn.setOnClickListener(this);
        test2Btn=findViewById(R.id.test2Btn);
        test2Btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      int btnId= view.getId();

      switch (btnId)
      {
          case R.id.test1Btn:
             myTest1();
              break;
          case R.id.test2Btn:
              myTest2();
              break;


      }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE1) {
            String receivedData = data.getStringExtra("return_result_tag");

            if (resultCode == RESULT_OK)
                feedback.setText(receivedData);
            else
                feedback.setText("Canceled");
        }

    }

    private void myTest1() {

      Intent myIntent= new Intent(this,Test1.class);
      startActivityForResult(myIntent,REQUEST_CODE1);

    }
    private void myTest2() {
        Intent myIntent= new Intent(this,Test2.class);
        startActivityForResult(myIntent,REQUEST_CODE2);
    }
}
