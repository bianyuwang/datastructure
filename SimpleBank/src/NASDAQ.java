import java.util.ArrayList;
import java.util.Iterator;

public class NASDAQ implements Subject {
    public int value;

    public ArrayList<Observor> observorList= new ArrayList<Observor>();

    @Override
    public void attach(Observor observor) {
        observorList.add(observor);
    }

    @Override
    public void detach(Observor observer) {
        if (this.observorList.contains(observer)) {
            Iterator<Observor> iter = this.observorList.iterator();
            while (iter.hasNext()) {
                Observor item = iter.next();
                if (item.equals(observer)) {
                    iter.remove();
                }
            }
        }

    }

    @Override
    public void notify(String message) {
        for(Observor observer:observorList)
        {
            observer.update(message);
        }
    }
}
