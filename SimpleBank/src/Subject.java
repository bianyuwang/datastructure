public interface Subject {
    public void attach(Observor observer);
    public  void detach(Observor observer);
    public void notify(String message);
}
