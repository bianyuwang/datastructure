public interface Observor {
    public  void update(String message);
}
