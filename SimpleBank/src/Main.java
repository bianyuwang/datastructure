import java.util.Random;

public class Main {



    public static void main(String[] args)

    {

        Bank myBank= Bank.getInstansOfBank("my Bank");
        System.out.println(myBank.name);
        Bank myOtherBank= Bank.getInstansOfBank("my other bank");
        System.out.println(myOtherBank.name);


        Customer c1= new Customer("Customer one");
        Customer c2= new Customer("Customer two");
        Customer c3= new Customer("Customer three");

        Gis gis=new Gis();
        MutualFund mutualFund=new MutualFund();

        c1.setInvestment(gis);

        c1.investment.investment();

        c2.investment.investment();

        c2.setInvestment(mutualFund);
        c2.investment.investment();
        c3.investment.investment();

        myBank.addCustomer(c1);
        myBank.addCustomer(c2);
        myBank.addCustomer(c3);

        for (Customer c:myBank.customerList
             ) {
          System.out.println(c.name+" : ");
         c.investment.investment();

        }

        NasdaqRamdomValue nasdaqRamdomValue=new NasdaqRamdomValue();
        nasdaqRamdomValue.nasdaq.attach(c1);
        nasdaqRamdomValue.nasdaq.attach(c2);
        nasdaqRamdomValue.nasdaq.attach(c3);


        DowJonesRamdomValue dowJonesRamdomValue=new DowJonesRamdomValue();
        dowJonesRamdomValue.dowJones.attach(c1);
        dowJonesRamdomValue.dowJones.attach(c2);

        Thread thread1 = new Thread(nasdaqRamdomValue);
        Thread thread2 = new Thread(dowJonesRamdomValue);

        thread1.start();
        thread2.start();


    }
}
