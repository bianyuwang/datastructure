public class Customer implements Observor {

    public Investment investment;
    public String name;
Customer(String name)
{
    this.name=name;
    this.investment=new Normal();
}

    public void setInvestment(Investment investment){
        this.investment=investment;
    }

    @Override
    public void update(String message) {
        System.out.println(name+ "::::: Alert Message : Stock Changed::::"+message);
    }
}
