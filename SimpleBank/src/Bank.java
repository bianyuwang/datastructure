import java.util.ArrayList;
import java.util.Iterator;

public class Bank {

    public static Bank bank;
    public ArrayList<Customer> customerList;
    public String name;
    private Bank(String name) {
     this.name=name;
     this.customerList=new ArrayList<Customer>();
    }

    public void addCustomer (Customer customer){
      customerList.add(customer);
    }

    public void removeCustomer(Customer customer){
        if (this.customerList.contains(customer)) {
            Iterator<Customer> iter = this.customerList.iterator();
            while (iter.hasNext()) {
                Customer item = iter.next();
                if (item.equals(customer)) {
                    iter.remove();
                }
            }
        }
   }


    public static Bank getInstansOfBank(String name){

        if(bank==null)
        {
            bank=new Bank(name);
        }
        return bank;
    }

    public ArrayList<Customer> getCustomerList()
    {
        return this.customerList;
    }

}
