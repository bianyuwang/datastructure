import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter a number in Hex:");
        String hexNumber = sc.nextLine();

        double decNum = HexToDex(hexNumber);
        System.out.println("decimal is :" + decNum);

    }

    public static double HexToDex(String hexNum) {
        int j = hexNum.length() - 1;
        double sum = 0;
        for (int i = 0; i < hexNum.length(); i++) {
            if (hexNum.charAt(i) <='9') {
                sum = sum + (hexNum.charAt(i) - '0') * Math.pow(16, j);
            } else {
                sum = sum + (hexNum.charAt(i) - 'a'+10) * Math.pow(16, j);
            }
            j--;
        }
        return sum;
    }
}
