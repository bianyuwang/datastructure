package com.example.dialog.ToastEntity;

import android.animation.Animator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.example.dialog.R;

public class ToastCustom {


    Toast toast;
    private Context context;
    public View view;

    public ToastCustom(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
        this.toast=new Toast(context);

        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.view= inflater.inflate(R.layout.colored_toast_layout,null);

        toast.setView(view);
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public void setText(String text) {
    }

    public Toast getToast() {
        return toast;
    }

    }

