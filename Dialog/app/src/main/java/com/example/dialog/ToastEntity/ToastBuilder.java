package com.example.dialog.ToastEntity;

import android.content.Context;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ToastBuilder {

    public static Toast alert(Context context,String text,int duration)
    {
        ToastCustom toastCustom= new ToastCustom(context);
        toastCustom.setText(text);
        toastCustom.getToast().setDuration(duration);
        toastCustom.getToast().setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL,0,50);
        toastCustom.getView().setBackgroundColor(0xff44336);

        return toastCustom.getToast();




    }

    public  static Toast imageToast(Context context,int id, int duration)
    {
        Toast toast = new Toast(context);

        LinearLayout layout=new LinearLayout(context);
        ImageView lv=new ImageView(context);

        layout.setLayoutParams(new LinearLayout.LayoutParams(850,650));
        layout.addView(lv);

        lv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        lv.requestLayout();
        lv.setLayoutParams(layout.getLayoutParams());
        lv.setImageResource(id);

        toast.setView(layout);
        toast.setDuration(duration);
        toast.setGravity(Gravity.TOP|Gravity.CENTER,0,500);
        return toast;


    }


}
