package com.example.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class DialogActivity extends AppCompatActivity implements View.OnClickListener, DialogInterface.OnClickListener {

    Button implementation1_btn, implementation2_btn,radio_btn,selection_btn,progress_btn,custom_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        initialize();


    }

    private void initialize() {
    implementation1_btn=findViewById(R.id.implementation1_btn);
    implementation1_btn.setOnClickListener(this);
    implementation2_btn=findViewById(R.id.implementation2_btn);
        implementation2_btn.setOnClickListener(this);
    radio_btn=findViewById(R.id.radio_btn);
        radio_btn.setOnClickListener(this);
    selection_btn=findViewById(R.id.selection_btn);
        selection_btn.setOnClickListener(this);
    progress_btn=findViewById(R.id.progress_btn);
        progress_btn.setOnClickListener(this);
    custom_btn=findViewById(R.id.custom_btn);
        custom_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        int btnId =view.getId();

        switch(btnId)
        {
            case R.id.implementation1_btn:
                implementation1Alert();
                break;
            case R.id.implementation2_btn:
               implementation2Alert();
                    break;
            case R.id.radio_btn:
            singleChoice();
                break;
            case R.id.selection_btn:
                break;
            case R.id.progress_btn:
                progressDemo();
                break;
            case R.id.custom_btn:
                customDemo();
                break;


        }


    }

    private void customDemo() {

        Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.activity_dialog);
        dialog.show();
    }


    private void progressDemo() {
        ProgressDialog progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("Progress Demo");
progressDialog.setTitle("Please wait");

progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
progressDialog.show();
    }
    private void singleChoice() {

        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("AlertDialgo\njust a question for demo alert dialog")

                .setCancelable(false)
                .setSingleChoiceItems(new String[]{"A", "B", "C"}, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                     Toast.makeText(DialogActivity.this,"id="+i,Toast.LENGTH_SHORT).show();
                    }
                });

     alertDialogBuilder.setNegativeButton("No",this);
     alertDialogBuilder.setPositiveButton("Yes",this);
     alertDialogBuilder.setNeutralButton("Neutral",this);

        alertDialogBuilder.show();

    }



 private void implementation2Alert() {

        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("AlertDialgo\njust a question for demo alert dialog")
                .setMessage("Do you want to delete the file?")
                .setCancelable(false)
                .setIcon(android.R.drawable.sym_def_app_icon);

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(DialogActivity.this,"id="+i,Toast.LENGTH_SHORT).show();
            }});
        alertDialogBuilder.setPositiveButton("Yes",null);
        alertDialogBuilder.setNeutralButton("Neutral",null);


        alertDialogBuilder.show();
    }

    private void implementation1Alert() {
        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("this is an alert dialog");

        alertDialogBuilder.setNegativeButton("No",this);
        alertDialogBuilder.setPositiveButton("Yes",this);
        alertDialogBuilder.setNeutralButton("Neutral",this);

        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int which) {

        switch (which)
        {
            case DialogInterface.BUTTON_POSITIVE:
            case DialogInterface.BUTTON_NEUTRAL:
            case DialogInterface.BUTTON_NEGATIVE:
                Toast.makeText(DialogActivity.this,"id="+which,Toast.LENGTH_SHORT).show();
            break;
        }

    }
}
