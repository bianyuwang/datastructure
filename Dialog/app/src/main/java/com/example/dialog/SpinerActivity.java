package com.example.dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;

import com.example.dialog.MealEntity.MealRate;

import java.util.ArrayList;
import java.util.List;

public class SpinerActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    ImageView imageView;
    Button button1,button2;
    Spinner spinnerFood;
    RatingBar ratingBarMeal;
    List<MealRate> listOfMealRate=new ArrayList<>();

    String[] mealList={"Poutine","Salmon","Sushi","Tacos","chefPoutine"};
    int[] mealImage={R.drawable.poutine,R.drawable.salmon,R.drawable.sushi,R.drawable.tacos,R.drawable.chefpoutine};
    ArrayAdapter<String> mealAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spiner);

        initialize();

    }

    private void initialize() {

   imageView=findViewById(R.id.imageFood);
     spinnerFood=findViewById(R.id.spinner);
   spinnerFood.setOnItemSelectedListener(this);

 mealAdapter=new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,mealList);
   spinnerFood.setAdapter(mealAdapter);

       ratingBarMeal=findViewById(R.id.ratingBar);

        button1=findViewById(R.id.button1);
        button1.setOnClickListener(this);
        button2=findViewById(R.id.button2);
        button2.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        int id=view.getId();

        if(id==R.id.button2)
        {
            String meal=spinnerFood.getSelectedItem().toString();
            int rating=(int) ratingBarMeal.getRating();

            MealRate mealRate= new MealRate(meal,rating);
            listOfMealRate.add(mealRate);
            ratingBarMeal.setRating(0);


        }

        else
        {
            Bundle myBundle= new Bundle();
            myBundle.
            Intent myintent = new Intent(this,ShowAllActivity.class);

        }

    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long id) {


        int image=mealImage[i];
        imageView.setImageResource(image);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
