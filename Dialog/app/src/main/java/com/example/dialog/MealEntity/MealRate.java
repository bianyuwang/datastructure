package com.example.dialog.MealEntity;

public class MealRate {
    private String Name;
    private int rate;

    public MealRate() {
    }

    public MealRate(String name, int rate) {
        Name = name;
        this.rate = rate;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
