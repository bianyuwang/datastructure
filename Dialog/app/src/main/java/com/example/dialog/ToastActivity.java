package com.example.dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.dialog.ToastEntity.ToastBuilder;

public class ToastActivity extends AppCompatActivity implements View.OnClickListener {

    Button button1,button7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);
        button1=findViewById(R.id.toast1);
        button1.setOnClickListener(this);

        button7=findViewById(R.id.toast7);
        button7.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
int id=view.getId();
    if(id==R.id.toast7)
        ToastBuilder.imageToast(this,R.drawable.test,10);
    }
}
