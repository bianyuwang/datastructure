package com.example.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
Button dialog_btn, toast_btn, Spinner_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {

    dialog_btn=findViewById(R.id.Dialog_btn);
    dialog_btn.setOnClickListener(this);


        toast_btn=findViewById(R.id.Toast_btn);
        toast_btn.setOnClickListener(this);

        Spinner_btn=findViewById(R.id.Spinner_btn);
        Spinner_btn.setOnClickListener(this);




    }

    @Override
    public void onClick(View view) {
       int btnId=view.getId();
       switch(btnId){

           case R.id.Dialog_btn:
               openDialog();
               break;
           case R.id.Toast_btn:
               openToastDemo();
               break;
           case R.id.Spinner_btn:
               openSpinner();
               break;

       }

    }

    private void openSpinner() {


        Intent myIntent= new Intent(this,SpinerActivity.class);
        startActivity(myIntent);

    }

    private void openToastDemo() {

        Intent myIntent= new Intent(this,ToastActivity.class);
        startActivity(myIntent);

    }

    private void openDialog() {
        Intent myIntent= new Intent(this,DialogActivity.class);
        startActivity(myIntent);
    }
}
