package com.example.moviereference;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.moviereference.modal.Client;
import com.example.moviereference.modal.ListOfClients;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ShowAllActivity extends AppCompatActivity implements View.OnClickListener {

    TextView listViewClientTV;
    Button viewAllBtn;
    RadioGroup movieRadioGroup;
    ListOfClients clientList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all);
        intinialize();
        getMyIntent();
    }

    private void intinialize() {


        listViewClientTV=findViewById(R.id.ListViewClientTV);
        viewAllBtn=findViewById(R.id.ViewAllBtn);
        viewAllBtn.setOnClickListener(this);

        movieRadioGroup=findViewById(R.id.MovieRadioGroup);
    }

    private  void getMyIntent(){
        Bundle bundle=getIntent().getBundleExtra("intentBundle");
        Serializable bundleContent  =bundle.getSerializable("bundleClientList");
        clientList= (ListOfClients)bundleContent;

    }
    @Override
    public void onClick(View view) {
        for (Client c:clientList.getClientList()
             ) {
            listViewClientTV.append(c.toString()+"\n");

        }

        }
     }
