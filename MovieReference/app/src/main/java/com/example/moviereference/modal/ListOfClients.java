package com.example.moviereference.modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListOfClients implements Serializable {
    private List<Client> clientList=new ArrayList<Client>();

    public List<Client> getClientList() {
        return clientList;
    }


}
