package com.example.moviereference.modal;

import java.io.Serializable;

public class Client implements Serializable {
    private  int id;
    private String email;
    private String movie;



    public Client(int id, String email, String movie) {
        this.id = id;
        this.email = email;
        this.movie = movie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", movie='" + movie + '\'' +
                '}';
    }
}
