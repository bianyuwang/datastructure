package com.example.moviereference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.moviereference.modal.Client;
import com.example.moviereference.modal.ListOfClients;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button clearBtn,addBtn,removeBtn,updateBtn,showAllBtn;
    EditText clientNumET,clientEmailET;
    RadioGroup movieRG;

    ListOfClients listOfClients;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     initialize();



    }

    private void initialize() {
    clearBtn=findViewById(R.id.ClearBtn);
    clearBtn.setOnClickListener(this);
    addBtn=findViewById(R.id.AddBtn);
    addBtn.setOnClickListener(this);
    removeBtn=findViewById(R.id.RemoveBtn);
    removeBtn.setOnClickListener(this);
    updateBtn=findViewById(R.id.UpdateBtn);
    updateBtn.setOnClickListener(this);

    showAllBtn=findViewById(R.id.ShowAllBtn);
    showAllBtn.setOnClickListener(this);

    clientNumET=findViewById(R.id.ClientNumET);
    clientEmailET=findViewById(R.id.ClientEmailET);

    movieRG=findViewById(R.id.MovieRadioGroup);
    listOfClients=new ListOfClients();
    }

    @Override
    public void onClick(View view) {
        int btnID=view.getId();

        switch (btnID){

            case R.id.AddBtn:
                addClient();
                break;
            case R.id.ClearBtn:
                clearList();
                break;
            case R.id.RemoveBtn:
                removeClient();
                break;
            case R.id.UpdateBtn:
                updateClient();
                break;
            case R.id.ShowAllBtn:
                showAllClients();
                break;

        }


    }

    private void showAllClients() {
        Bundle bundle= new Bundle();
        bundle.putSerializable("bundleClientList",listOfClients);
        Intent intent=new Intent(this,ShowAllActivity.class);
        intent.putExtra("intentBundle",bundle);
        startActivity(intent);
    }

    private void updateClient() {
    }

    private void removeClient() {
    }

    private void clearList() {
    }

    private void addClient() {

        int id= Integer.valueOf(clientNumET.getText().toString());
     String email=clientEmailET.getText().toString();
      String movie="";
     int selectedRadioBtn=movieRG.getCheckedRadioButtonId();

    switch (selectedRadioBtn)
        {
            case R.id.ActionRB:
                movie="action";
                break;
            case R.id.AdventureRB:
                movie="adventure";
                break;
            case R.id.ComedyRB:
                movie="comedy";
                break;

        }


          Client client=new Client(id,email,movie);

          listOfClients.getClientList().add(client);
          Toast.makeText(this,"new client added",Toast.LENGTH_LONG).show();

    }
}
