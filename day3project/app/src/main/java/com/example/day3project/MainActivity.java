package com.example.day3project;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener {
   EditText name;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    public void initialize(){

        submit=findViewById(R.id.BtnSubmit);
        name=findViewById(R.id.name);
        submit.setOnClickListener(this);
// Approach 1 create an object
   /*     View.OnClickListener myOnclickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,
                        "Approach 1:Create and Press an object",
                Toast.LENGTH_LONG).show();
            }
        };
submit.setOnClickListener(myOnclickListener);
*/
//approach 2 anonymous inner class
/*
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,
                        "Approach 2 :Create and Press an object",
                        Toast.LENGTH_LONG).show();
            }
        });


  // approach 3 lambda expression
  submit.setOnClickListener((View view )->Toast.makeText(MainActivity.this,
                "Approach 3 :Create and Press an object",
                Toast.LENGTH_LONG).show());
*/

    }

// approach 4 :common way
    @Override
    public void onClick(View view) {

        Toast.makeText(MainActivity.this,
                "Approach 4 :Create and Press an object",
                Toast.LENGTH_LONG).show();
    }
}
