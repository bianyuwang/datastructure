import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);

        System.out.println("Please enter a word: ");
        String newWord=sc.nextLine();
        int low=0;
        int high= newWord.length()-1;
        boolean isPrime=true;
        while (low<high)
        {if(newWord.charAt(low)!=newWord.charAt(high))
        {  isPrime=false;
            break;
        }
         low++;
        high--;
        }

        String  message=isPrime?"This is a palindrome word":"This in not a palindrome word";
        System.out.println(message);
    }
}
