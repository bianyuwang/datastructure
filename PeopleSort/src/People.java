import java.util.Comparator;

public class People implements Comparable {
    private String name;
    private String family;
    private int id;
    static public int count=0;

    People(String name,String family){
       this.name=name;
       this.family=family;
       this.count++;
       this.id=count;
    }

    public int getId() {
        return id;
    }

    public String getFamily() {
        return family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public void setFamily(String family) {
        this.family = family;
    }


    @Override
    public int compareTo(Object o) {
      return  this.getId()-((People)o).getId();

    }

    public static class SortbyName implements Comparator{

        @Override
        public int compare(Object o1, Object o2) {
            return ((People)o1).getName().compareTo(((People)o2).getName());
        }
    }

    public static class SortbyFamilly implements Comparator{

        @Override
        public int compare(Object o1, Object o2) {
            return ((People)o1).getFamily().compareTo(((People)o2).getFamily());
        }
    }

    public static class SortbyId implements Comparator{

        @Override
        public int compare(Object o1, Object o2) {
            return ((People)o1).getId()-((People)o2).getId();
        }
    }
}


