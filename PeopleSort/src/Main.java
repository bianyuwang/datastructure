import java.util.*;


// 1/create a list of people 5 persons, sort this 5 persons by different approch
public class Main {

    public static void main(String[] args) {
        People p1 = new People("Jerry", "Mouse");
        People p2 = new People("Tom", "Cat");
        People p3 = new People("Spike", "Dog");
        People p4 = new People("Tyke", "Dog");
        People p5 = new People("Tuffy", "Cat");

        ArrayList<People> persons = new ArrayList<>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);
        persons.add(p5);

        Scanner sc = new Scanner(System.in);

        int choice = 1;

        do {
            System.out.println("please enter your choice:\n" +
                    "1.Comparable by Id\n" +
                    "2.Sort by Name using inner class\n" +
                    "3.Sort by Family using inner class\n" +
                    "4.Sort by Name using anonymous inner class\n" +
                    "5.Sort by Id using anonymous inner class\n" +
                    "6.Sort by id using lambda expression\n" +
                    "7.Sort by Name using lambda expression\n" +
                    "8.Sort by Family using lambda expression\n" +
                    "0. EXIT"
            );

            choice = sc.nextInt();

            switch (choice) {
                case 1:
                    ComparableById(persons);
                    break;
                case 2:
                    InnerClassSortByName(persons);
                    break;
                case 3:
                    InnerClassSortByFamily(persons);
                    break;
                case 4:
                    SortByNameAnonymousInnerClass(persons);
                    break;
                case 5:
                    SortByFamilyAnonymousInnerClass(persons);
                    break;
                case 6:
                    SortByIdUseLambda(persons);
                    break;
                case 7:
                    SortByNameUseLambda(persons);
                    break;
                case 8:
                    SortByFamilyUseLambda(persons);
                    break;
                case 0:
                    System.out.println("exit");
                    break;
                default:
                    System.out.println("Not a right choice!");
                    break;
            }

        } while (choice != 0);

    }

    public static void ComparableById(ArrayList<People> persons) {
        System.out.println("------comparable sort by id------");
        Collections.sort(persons);
        for (People p : persons) {
            System.out.println("id:" +  p.getId() + " Name: " + p.getName() + " Family name:" +  p.getFamily());
        }
    }

    public static void InnerClassSortByName(ArrayList<People> persons) {
        System.out.println("------ sort by Name using inner class------");
        Collections.sort(persons, new People.SortbyName());
        for (People p : persons) {
            System.out.println("Name: " + p.getName() + " Family name:" +  p.getFamily() + " id:" + p.getId());
        }
    }


    public static void InnerClassSortByFamily(ArrayList<People> persons) {
        System.out.println("------ sort by Family using inner class------");
        Collections.sort(persons, new People.SortbyFamilly());
        for (People p : persons) {
            System.out.println(" Family name:" +p.getFamily() + " id:" + p.getId() + " Name: " + p.getName());
        }

    }

    public static void SortByNameAnonymousInnerClass(ArrayList<People> persons) {
        System.out.println("------ sort by Name using anonymous inner class------");
        Collections.sort(persons, new Comparator<People>() {
            public int compare(People p1, People p2) {
                return (p1.getName()).compareTo(p2.getName());

            }
        });
        for (People p : persons) {
            System.out.println("Name: " +  p.getName() + " Family name:" + p.getFamily() + " id:" + p.getId());
        }

    }


    public static void SortByFamilyAnonymousInnerClass(ArrayList<People> persons) {

        System.out.println("------ sort by Family name using anonymous inner class------");

        Collections.sort(persons, new Comparator<People>() {
            public int compare(People p1, People p2) {
                return (p1.getFamily()).compareTo(p2.getFamily());

            }
        });

        for (People p : persons) {
            System.out.println(" Family name:" + p.getFamily() + " id:" + p.getId() + " Name: " + p.getName());
        }

    }

    public static void SortByIdAnonymousInnerClass(ArrayList<People> persons) {
        System.out.println("------ sort by id using anonymous inner class------");

        Collections.sort(persons, new Comparator<People>() {
            public int compare(People p1, People p2) {
                return (p1.getId() - p2.getId());
            }
        });

        for (People p : persons) {
            System.out.println("id:" +  p.getId() + " Name: " + p.getName() + " Family name:" + p.getFamily());
        }
    }

    public static void SortByIdUseLambda(ArrayList<People> persons) {

        System.out.println("------ sort by id using lambda expression------");
        Collections.sort(persons, (people1, people2) -> {
            return ((People) people1).getId() - ((People) people2).getId();
        });
        for (Object p : persons) {
            System.out.println("id:" + ((People) p).getId() + " Name: " + ((People) p).getName() + " Family name:" + ((People) p).getFamily());
        }


    }

    public static void SortByNameUseLambda(ArrayList<People> persons) {


        System.out.println("------ sort by name using lambda expression------");
        Collections.sort(persons, (people1, people2) -> {
            return ((People) people1).getName().compareTo(((People) people2).getName());
        });
        for (People p : persons) {
            System.out.println("id:" + p.getId() + " Name: " +  p.getName() + " Family name:" + p.getFamily());
        }

    }

    public static void SortByFamilyUseLambda(ArrayList<People> persons) {
        System.out.println("------ sort by Family name using lambda expression------");
        Collections.sort(persons, (people1, people2) -> {
            return ((People) people1).getFamily().compareTo(((People) people2).getFamily());
        });
        for (People p : persons) {
            System.out.println("id:" + p.getId() + " Name: " + p.getName() + " Family name:" + p.getFamily());
        }
    }
}
