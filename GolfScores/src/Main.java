import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Double> score= new ArrayList<>();
        System.out.println("This program reads golf scores and shows \nhow much each differs from the average");

        fillArrayList(score);
        showDifferebce(score);
    }

    private static void showDifferebce(ArrayList<Double> scorelist) {
        double avarage;
        avarage=  getAvarage(scorelist);

        for (Double d: scorelist
             ) {
            System.out.println("the differs is: "+ (d-avarage));
        }

    }

    private static double getAvarage(ArrayList<Double> scorelist) {
        double total=0;
        double avarage;
        for (Double d: scorelist
             ) {
            total=total+d;
        }

        avarage= total/scorelist.size();
        System.out.println("the avarage is: "+ avarage);
        return avarage;

    }

    private static void fillArrayList(ArrayList<Double> scoreList) {
        double score=1;
        System.out.println("Enter golf scores, negative number for finish:");
        Scanner s = new Scanner(System.in);

        while (score >0){

            score=s.nextDouble();
            if (score>0)
            scoreList.add(score);
    }

    }


}
