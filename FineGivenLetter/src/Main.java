
/*Write a recursive method that finds the number of occurrences of a specified letter
 in a string using the following method header:*/

import java.util.Scanner;

public class Main {
    public static int totalOcc=0;
    public static int place=0;
    public static void main(String[] args) {
        System.out.println("Enter a String");
        Scanner sc= new Scanner(System.in);
        String str= sc.nextLine();
        System.out.println("Which letter do you want to count:");
        String s = sc.nextLine();

        char[] chars = s.toCharArray();

        char a= chars[0];

        System.out.println("There is "+count( str,  a)+" '"+a+"' in the string :"+str);
    }

    public static int count(String str, char a){

         if (place==str.length())
         {return totalOcc;}
         else
         {
             if (str.charAt(place)== a)
                 totalOcc++;

             place++;
             return count(str,a);

         }

    }
}
