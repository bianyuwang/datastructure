import java.util.Scanner;

public class Main {

    public static void main(String[] args)

    {
        Scanner sc= new Scanner(System.in);

        System.out.println("enter a number");
       int num1=sc.nextInt();
        System.out.println("enter another number");
        int num2=sc.nextInt();
        System.out.println("Greater common divisor is:"+ getGCD(num1,num2));
    }


    public static int getGCD (int num1,int num2)
    {
        //return num2==0?num1:getGCD(num2,num1%num2);
        if(num1%num2==0)
            return num2;
        else
        {int temp=num1%num2;
        num1=num2;
        num2=temp;
        return getGCD(num1,num2);
        }
    }

}
