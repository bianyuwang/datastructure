package myRestWeb.rest;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myRestWeb.entity.Address;
import myRestWeb.entity.Payment;
import myRestWeb.entity.Customer;
import myRestWeb.exceptions.CustomerNotFoundException;


@RestController
@RequestMapping("/api")
public class CustomerRestController {

	List<Customer> theCustomers = new ArrayList<>();
	// Define end point for "/students" - which returns a list of students
	@PostConstruct
	public void intitialCustomers() {
		
		LocalDate dateOfBirth = LocalDate.of(2017, Month.MAY, 15);
		
		Customer c1=new Customer("Tom","Cat",dateOfBirth,null,"TomCat@gmail.com","0123456789","M",null);
		c1.setAddress(new Address(123,"no mouse","cat town","qc","canada","a1a1a1"));
		c1.setPayment(new Payment("master","123423412341234"));
		
		dateOfBirth = LocalDate.of(2011, Month.MAY, 12);
		Customer c2=new Customer("Tony","Cat",dateOfBirth,null,"TonyCat@gmail.com","9876543210","M",null);
		c2.setAddress(new Address(125,"no mouse","cat town","qc","canada","a1a1a1"));
		c2.setPayment(new Payment("master","432143214321"));
		
	
		dateOfBirth = LocalDate.of(2011, Month.JANUARY, 26);
		Customer c3=new Customer("Jacob","Mouse",dateOfBirth,null,"JacobMouse@gmail.com","345678898","F",null);
		c3.setAddress(new Address(657,"no cat","mouse town","qc","canada","c1c1c1"));
		c3.setPayment(new Payment("master","1234432112344321"));
		
		dateOfBirth = LocalDate.of(2012, Month.DECEMBER, 26);
		Customer c4=new Customer("Jerry","Mouse",dateOfBirth,null,"JerryMouse@gmail.com","2345678910","M",null);
		c4.setAddress(new Address(345,"no cat","mouse town","qc","canada","c1c1c1"));
		c4.setPayment(new Payment("master","1234432112344321"));
		
		dateOfBirth = LocalDate.of(2015, Month.APRIL, 16);
		Customer c5=new Customer("MiuMiu","Cat",dateOfBirth,null,"MiuMiuCat@gmail.com","1234567890","F",null);
		c5.setAddress(new Address(128,"no mouse","cat town","qc","canada","b1b1b1"));
		c5.setPayment(new Payment("master","1234432112344321"));
		
		theCustomers = new ArrayList<>();
		theCustomers.add(c1);
		theCustomers.add(c2);
		theCustomers.add(c3);
		theCustomers.add(c4);
		theCustomers.add(c5);
	}
	@GetMapping("/customers")
	
	public List<Customer> getCustomers() {

		return theCustomers;
	}
	
	@GetMapping("/customers/{customerId}")
public Customer getCustomer(@PathVariable int customerId) {	
		
		if(customerId>=theCustomers.size()||customerId<0)
			throw new CustomerNotFoundException("Customer id not found -  " + customerId);
		return theCustomers.get(customerId);
	}
	
	
	@GetMapping("/customers/city/{someCity}")
	public List<Customer> getCustomersByCity(@PathVariable String someCity)
	{
		List<Customer> customersByCity =new ArrayList<Customer>();
		

	   for (Customer c : theCustomers) {
			if (c.getAddress().getCity().equals(someCity))
		
				customersByCity.add(c);
			}
	
		if(customersByCity.size()>0)
			
		return customersByCity;
		else {
			throw new CustomerNotFoundException("Customer in "+someCity +" not found");
		}
		}

	
	@GetMapping("/customers/sorted/family")
	public List<Customer> getCustomersSortedByFamily(){
		 Collections.sort(theCustomers, (c1, c2) -> {
			  return ((Customer) c1).getFamily().compareTo(((Customer) c2).getFamily());
	        });
		 return theCustomers;
	}
	
	@GetMapping("/customers/sorted/birthday")
	public List<Customer> getCustomersSortedByDob(){
		 Collections.sort(theCustomers, (c1, c2) -> {
			  return ((Customer) c1).getBirthdate().isAfter(((Customer) c2).getBirthdate())?-1:1;
	        });
		 return theCustomers;
	}
	
	
	@GetMapping("/customers/find/family/{family}")
	public List<Customer> getCustomersByFamily(@PathVariable String family){
		
		List<Customer> customersByFamily =new ArrayList<Customer>();
		

		   for (Customer c : theCustomers) {
				if (c.getFamily().toLowerCase().equals(family.toLowerCase()))
			
					customersByFamily.add(c);
				}
		
			if(customersByFamily.size()>0)
				
			return customersByFamily;
			else {
				throw new CustomerNotFoundException("Customer in the family -- "+family +" not found");
			}
	
	}
	
	@GetMapping("/customers/find/name/{name}")
public List<Customer> getCustomersByName(@PathVariable String name){
	
		List<Customer> customersByName =new ArrayList<Customer>();

		   for (Customer c : theCustomers) {
				if (c.getName().toLowerCase().contains(name))
			
					customersByName.add(c);
				}
		
			if(customersByName.size()>0)
				
			return customersByName;
			else {
				throw new CustomerNotFoundException("Customer name -- "+name +" not found");
			}
	
	}
	
	
	
	
	
}
