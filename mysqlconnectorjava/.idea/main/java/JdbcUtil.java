import java.sql.*;

public class JdbcUtil {

    /**
     * 用于获取Connection对象，创建连接
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        // 数据库地址
        String url = "jdbc:mysql://localhost:3306/jdbc_data?characterEncoding=utf8&useSSL=false&serverTimezone=UTC";
        // 数据库用户名
        String user = "root";
        // 数据库密码
        String password = "root";
        // mysql驱动
        String driverClass = "com.mysql.cj.jdbc.Driver";

        Class.forName(driverClass);
        Connection connection = DriverManager.getConnection(url, user, password);
        return connection;
    }

    /**
     * 销毁连接
     * @param resultSet
     * @param statement
     * @param connection
     */
    public static void release(ResultSet resultSet, Statement statement, Connection connection){
        if(resultSet != null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}