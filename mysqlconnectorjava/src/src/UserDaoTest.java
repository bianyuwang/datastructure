import org.junit.jupiter.api.Test;

import java.util.List;

public class UserDaoTest {
    @Test
    public void findTest(){
        UserDao dao = new UserDao();
        List<user> users = dao.find();
        System.out.println("一共查询到" + users.size() + "个用户。");
        for (user user: users) {
            System.out.println(user);
        }
    }

    /**
     * 保存一个用户的信息
     */
    @Test
    public void saveTest(){
        UserDao dao = new UserDao();
        user user = new user();
        user.setName("LiMing");
        user.setAge(25);
        dao.save(user);
        System.out.println("保存用户信息成功！");
    }


}
