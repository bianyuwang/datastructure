import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDao {


    /**
     * 查询user表中的所有数据
     * @return
     */
    public List<user> find() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        // 查询sql语句
        String sql = "SELECT id, name, age FROM user_jdbc";
        List<user> users = new ArrayList<>();
        try {
            connection = JdbcUtil.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            // resultSet中保存着我们要取出的数据
            resultSet = preparedStatement.executeQuery();
            // 遍历，取出数据并保存在user对象中，再保存在users集合中
            while (resultSet.next()) {
                user user = new user();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setAge(resultSet.getInt("age"));
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 使用后销毁连接
            JdbcUtil.release(resultSet, preparedStatement, connection);
        }
        return users;
    }

    /**
     * 添加一条数据到数据库
     * @param user
     */
    public void save(user user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // 使用“?”占位符来插入参数以避免sql注入
        String sql = "INSERT INTO user_jdbc(name, age) VALUES(?, ?)";
        try {
            connection = JdbcUtil.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            // 将传入的user中的参数加入到参数列表中
            preparedStatement.setString(1, user.getName());
            preparedStatement.setInt(2, user.getAge());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.release(null, preparedStatement, connection);
        }
    }

}
