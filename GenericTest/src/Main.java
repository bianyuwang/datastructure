import java.util.Scanner;

public class Main {

    public static void main(String[] args)

    {
        System.out.println("Test of Generic Class Pair!");

        Pair<String> secretPair= new Pair<>("Happy", "Day");

        Scanner s =new Scanner(System.in);


        System.out.println("Input the first word:");


        String inputFirst=s.nextLine();

        System.out.println("Input the second word:");

        String inputSecond=s.nextLine();

        Pair<String> inputPair= new Pair<>(inputFirst,inputSecond);

        if(secretPair.equals(inputPair)){

            System.out.println("You got the right pair!");
        }

        else
        {
            System.out.println("You got the wrong pair, right pair is :"+secretPair.toString());

            System.out.println("first word in secret pair is : "+secretPair.getFirst());
            System.out.println("your first word in pair is : "+inputPair.getFirst());
            System.out.println("second word in secret pair is : "+secretPair.getSecond());
            System.out.println("your second word in pair is : "+inputPair.getSecond());

        }
    }
}
