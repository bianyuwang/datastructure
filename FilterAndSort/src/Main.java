import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {


    public  static  ArrayList<Empoyee> prepareData(){

        ArrayList<Empoyee> employeeList =new ArrayList<Empoyee>();
        employeeList.add(new Empoyee("Jerry","Zhang",2000,true));
        employeeList.add(new Empoyee("Tom","Ford",9000,true));
        employeeList.add(new Empoyee("Will","Smith",4000,true));
        employeeList.add(new Empoyee("Justin","Bieber",6000,true));
        employeeList.add(new Empoyee("Justin","Timberlake",2000,false));
        employeeList.add(new Empoyee("Mary","Queen",5000,true));
        employeeList.add(new Empoyee("Bill","Gates",8000,true));
        employeeList.add(new Empoyee("Mark","Zuckerberg",7000,true));
       return  employeeList;
    }
   public static void main(String[] args) {
       ArrayList<Empoyee> empoyeesList=prepareData();

     int salary=3000;
   //   Collections.sort(empoyeesList,(e1,e2)->{return e1.getSalary()-e2.getSalary();});
      empoyeesList.stream()
              .filter((e) -> e.getSalary()>salary&& e.getMarried()==true)
              .sorted((e1,e2)->{return e1.getSalary()-e2.getSalary();})
              .limit(5)
          //    .forEach(System.out::println);
            .forEach(e ->System.out.println("Full name: "+e.getName()+" "+
                     e.getFamily()+" is married, salary is over 3000: "+e.getSalary()));
           //     .collect(Collectors.toCollection(ArrayList::new));

       empoyeesList.stream().sorted(Comparator.comparing(Empoyee::getSalary))
               .forEach(System.out::println);

       Optional<Integer> sumOfSalaray;

       sumOfSalaray= empoyeesList.stream().map(Empoyee::getSalary).reduce((e1,e2)->e1+e2);
       sumOfSalaray.ifPresent(System.out::println);


    }
}
