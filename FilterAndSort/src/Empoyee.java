public class Empoyee {
    private  String name;
private String family;
private int salary;
private boolean isMarried;

Empoyee(String name,String family,int salary,boolean isMarried)

{
    this.name=name;
    this.family=family;
    this.salary=salary;
    this.isMarried=isMarried;
}
    public void setName(String name) {
        this.name = name;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public void setMarried(boolean married) {
        isMarried = married;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public String getFamily() {
        return family;
    }

    public boolean getMarried(){
    return  isMarried;
    }



    @Override
    public String toString() {
        return "Empoyee{" +
                "name='" + name + '\'' +
                ", family='" + family + '\'' +
                ", salary=" + salary +
                ", isMarried=" + isMarried +
                '}';
    }
}
