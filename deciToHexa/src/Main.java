import java.util.Scanner;

public class Main {

    public static void main(String[] args)

    {
        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter a number in Decimal:");
        int decimalNumber = sc.nextInt();
        String hexNum=DecToHex(decimalNumber);
        System.out.println("Hex is :"+ hexNum);
    }

    public static String DecToHex(int decNum)
    {
     String hexNum = "";
       int remainder;
        while(decNum!=0)
        {
            decNum=decNum/16;
            remainder=decNum%16;
            char c=(remainder>10)?(char)(65+(remainder-10)):(char)(48+remainder);
            hexNum=c+hexNum;
        }
      return hexNum;
    }
}
