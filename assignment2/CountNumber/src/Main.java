import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        Map<Integer, Integer> countOccu = new HashMap<Integer, Integer>();
        System.out.println("Enter a number (between 1 and 100)：");
        int keyNumber = sc.nextInt();
        if (keyNumber <= 100 && keyNumber >= 0) {
            while (keyNumber != 0) {


                if (countOccu.containsKey(keyNumber))
                    countOccu.replace(keyNumber, countOccu.get(keyNumber) + 1);

                else
                    countOccu.put(keyNumber, 1);

                System.out.println("Enter a number：");
                keyNumber = sc.nextInt();
                if (keyNumber>100 || keyNumber <0)
                    break;
            }
            countOccu.forEach((k, v) -> System.out.println("Number : " + k + " Count : " + v));

        } else {
            System.out.println("Number out of range!");
        }
    }
}