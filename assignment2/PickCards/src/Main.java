import java.util.Random;
import java.util.Scanner;

/*
* Say you want to write a program that will pick four cards at random from a deck of 52 cards.
*  All the cards can be represented using an array named deck, filled with initial values 0 to 51,
* as follows: Card numbers 0 to 12, 13 to 25, 26 to 38, and 39 to 51 represent 13 Spades, 13 Hearts,
* 13 Diamonds, and 13 Clubs, respectively, as shown in Figure 7.2. cardNumber / 13 determines the suit
* of the card and cardNumber % 13 determines the rank of the card. After shuffling
* the array deck, pick the first four cards from deck. The program displays the cards from these four card numbers.*/
public class Main {

    public static void main(String[] args) {

        int cards[]= new int[52];
      //initial cards
        for(int i =0;i<52;i++)
        {
            cards[i]=i;
        }

        Random rgen = new Random();  // Random position generator
System.out.println("press any key to pick four Cards...");
        Scanner sc=new Scanner(System.in);
        sc.next();
        cards=ShuffleCards(cards);
        String suit;

        for(int i=0;i<4;i++)
        {
            switch (cards[i]/13)
            { case 0:
                suit="Spade";
                break;
              case 1:
                    suit="Heart";
                    break;
                case 2:
                    suit="Diamond";
                    break;
                case 3:
                    suit="Club";
                    break;
                default:
                    suit="something wrong";
                    break;

            }
          System.out.println("card No."+i+": "+suit + " "+cards[i]%13);

        }


    }

   // Shuffle Cards
    public static int[] ShuffleCards(int[] cards){
        Random rgen = new Random();  // Random position generator

        for (int i=0; i<cards.length; i++) {
            int randomPosition = rgen.nextInt(cards.length);
            int temp = cards[i];
            cards[i] = cards[randomPosition];
            cards[randomPosition] = temp;
        }

        return cards;
    }


}
