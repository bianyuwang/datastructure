import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        System.out.println(" Enter list entries, when prompted.");


        ArrayList<String> toDoList = new ArrayList<>(20);
        boolean done=true;
        Scanner s = new Scanner(System.in);
        String answer;

        while (done){
            System.out.println(" Input an entry:");
            String toDo=s.nextLine();
            toDoList.add(toDo);
            System.out.println("More items for the list? yes/no");

            answer=s.nextLine();
            if (answer.equalsIgnoreCase("no"))
                done=false;

        }
        System.out.println("The List Contains");

        for (String str: toDoList
             ) {
            System.out.println(str);
        }

    }
}
