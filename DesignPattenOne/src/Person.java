import java.util.ArrayList;
import java.util.Iterator;

public abstract class Person {
    ArrayList<LanguageSpoken> languageSpoken;

    public String nationality;

    public abstract void appearance();

    public void addLanguageSpoken(LanguageSpoken languageSpoken) {
        this.languageSpoken.add(languageSpoken);
    }

    public void removeLanguageSpoken(LanguageSpoken languageSpoken) {
        if (this.languageSpoken.contains(languageSpoken)) {
            Iterator<LanguageSpoken> iter = this.languageSpoken.iterator();
            while (iter.hasNext()) {
                LanguageSpoken item = iter.next();
                if (item.equals(languageSpoken)) {
                    iter.remove();
                }
            }

        }

    }
}