public class Main {

    public static void main(String[] args) {
       Person Yangyang=new ChinaCitizen();
       Yangyang.appearance();
       System.out.println("----Yangyang can speak chinese");
        for (LanguageSpoken l:Yangyang.languageSpoken ) {
         l.speak();
        }

        System.out.println("----Now Yangyang can speak chinese and english");
        English english=new English();
        Yangyang.addLanguageSpoken(english);

        for (LanguageSpoken l:Yangyang.languageSpoken ) {
            l.speak();
        }

      Person Mary=new CanadaCitizen();
        Mary.appearance();
        System.out.println("----Mary from canada can speak english");
        for (LanguageSpoken l:Mary.languageSpoken ) {
            l.speak();
        }
        System.out.println("----Vincent from Quebec can speak english and french");
        Person VincentFromQuebec =new CanadaCitizen();
        VincentFromQuebec.appearance();
        Francais francais=new Francais();
        VincentFromQuebec.addLanguageSpoken(francais);
        for (LanguageSpoken l:VincentFromQuebec.languageSpoken ) {
            l.speak();
        }

        VincentFromQuebec.addLanguageSpoken(new German());
        VincentFromQuebec.removeLanguageSpoken(francais);
        System.out.println("----Vincent from Quebec now can speak english and german");
        for (LanguageSpoken l:VincentFromQuebec.languageSpoken ) {
            l.speak();
        }


    }
}
