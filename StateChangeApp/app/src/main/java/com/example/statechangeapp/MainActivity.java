package com.example.statechangeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        System.out.println("-----------------onRestart : ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("-----------------onStart : ");
    }

    @Override
    protected void onStop() {
        super.onStop();

        System.out.println("-----------------onStop : ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("-----------------onResume : ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("-----------------onPause : ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("-----------------onDestroy : ");
    }
}
