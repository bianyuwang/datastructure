public class DatabaseConnexion {
    public static DatabaseConnexion UniqueConnexion;
    private String database;
    private String psw;

    public String getDatebase() {
        return database;
    }

    private DatabaseConnexion(String database,String psw)
        {
               this.database=database;
               this.psw=psw;
        }

        public static DatabaseConnexion getConnection(String database,String psw){
            if (UniqueConnexion==null) {
                UniqueConnexion = new DatabaseConnexion(database, psw);
            }
             return UniqueConnexion;
        }

}
