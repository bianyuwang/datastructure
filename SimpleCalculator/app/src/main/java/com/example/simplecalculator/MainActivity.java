package com.example.simplecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editText1, editText2;
    TextView sumTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private  void initialize(){
        editText1=findViewById(R.id.editText1);
        editText2=findViewById(R.id.editText2);
        sumTextView=findViewById(R.id.sum);
    }

    public void doSum(View view){

        String editText1Str=editText1.getText().toString();
        String editText2Str=editText2.getText().toString();

        int editText1Int=Integer.valueOf(editText1Str);
        int editText2Int=Integer.valueOf(editText2Str);

        int sum=editText1Int+editText2Int;

        Toast.makeText(this,String.valueOf(sum),Toast.LENGTH_LONG).show();

    }
}
